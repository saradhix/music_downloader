import requests
import sys
import os
import bs4 as bs
import time

def main():
    homepage ='http://mp3teluguwap.net/zip/'

    r = requests.get(homepage)
    print(r.status_code)
    soup = bs.BeautifulSoup(r.text,'lxml')
    links = soup.find_all('a')
    for link in links:
        name = link.get_text().strip()
        url = link.get('href')
        if '.zip' in name:
            download_album(name, url)


def download_album(album, url):
    baseurl = 'http://mp3teluguwap.net/zip/'
    print("Entered with album=", album, "url=", url)
    album = album.replace(' ','-')
    album = album.replace('(', '')
    album = album.replace(')', '')
    album = album.replace('[', '')
    album = album.replace(']', '')
    filename = url.split('/')[-1] 
    path='../songs/'+album
    url = baseurl+url
    command = "wget '"+url+"' -O " + path
    if not os.path.exists(path):
        print("Running command", command)
        os.system(command)
        time.sleep(30)
    else:
        print(path, "Exists. Hence skipping")






if __name__ == "__main__":
    main()


