import requests
import bs4 as bs
import sys
import os
import time

baseurl = 'https://a2z3gp.com/Telugu%20HQ%20320kbps%20Songs/'
homepage='https://a2z3gp.com/Telugu%20HQ%20320kbps%20Songs/?d=a%20to%20z%20telugu%20mp3%20320kbps'
homepage='https://a2z3gp.com/Telugu%20HQ%20320kbps%20Songs/?d=a%20to%20z%20telugu%20mp3%20320kbps&n=8&c=8&s=0'
homepage='https://a2z3gp.com/Telugump3%20Songs/?d=A%20TO%20Z%20TELUGU%20HQ%20MP3%20SONGS&n=18&c=6&s=0'
homepage='https://hindimp3.a2z3gp.com/wx/Hindi+MP3/Hindi+Old+MP3/Best+Of+90s+VHQ-2.html'
base_dir = '../a2zsongshindi'
def main():

    r = requests.get(homepage)
    #print(r.text)
    print(r.status_code)


    soup = bs.BeautifulSoup(r.text,'lxml')

    print("Title", soup.title)
    links = soup.find_all('a')


    for link in links:
        #print("Link=",link.get('href'))
        #continue
        if '/wap/Hindi+' in link.get('href'):
            album = link.get_text().strip()
            #url = baseurl + link.get('href')
            url = link.get('href')
            print("URL=", url, "page=", album)
            #sys.exit()
            #download_page(url, album)

def download_page(url, page):
    url = url.replace("c=8","c=1000")  
    #Above hack is ito ensure all albums belonging to this 
    #category page come in 1 shot instead of pagination by 8 
    print("Downloading albums from  the page url", url)
    r = requests.get(url)
    print(r.status_code)
    soup = bs.BeautifulSoup(r.text,'lxml')
    links = soup.find_all('a')
    for link in links:
        if 'kbps' in link.get('href'):
            album = link.get_text().strip()
            url = link.get('href')
            print("URL=|", url, "|page=", album)
            download_album(url, album)

def download_album(url, album):
    album = album.replace('(','').replace(')','').replace('[','').replace(']','').replace(' ','_')
    print("Downloading album", album, "from", url)
    content = requests.get(baseurl+url).text
    soup = bs.BeautifulSoup(content, 'lxml')

    links = soup.find_all('a')
    for link in links:
        song = link.get_text().strip()
        url = link.get('href')
        if url.endswith('.mp3'):
            #print("Song=", song, "url=", url)
            try:
                os.mkdir(base_dir+'/'+album)
            except:
                pass
            download_song(baseurl+url, album, song)


def download_song(url, album, song):
    print("Download ", song, "from album", album,"from url", url)
    #Modify the url to replace these things
    #download.php?d= should be replaced with to load/
    #&filename= should be replaced with /

    url = url.replace("download.php?d=","load/")
    url = url.replace("&filename=", "/")
    song = song.replace(' ', '_')+'.mp3'
    song = song.replace('(','').replace(')','').replace('[','').replace(']','').replace(' ','_')
    #print("Replaced url=", url, "song=",song)
    path = base_dir+'/'+album+'/'+song
    #print("Downloading to path", path)
    command = "wget '"+url+"' -O " + path
    if not os.path.exists(path):
        #print("Running command", command)
        os.system(command)
        time.sleep(5)
    else:
        print(path, "Exists. Hence skipping")


if __name__ == "__main__":
    #download_album('Ala', 'https://maamusiq.com/albums/ala-vaikunthapurramuloo-2019/')
    main()
