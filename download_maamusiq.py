import requests
import bs4 as bs
import sys
import os
import time
from random import randint

def main():
    homepage='https://maamusiq.com/a-to-z-index/'

    r = requests.get(homepage)
    #print(r.text)
    print(r.status_code)


    soup = bs.BeautifulSoup(r.text,'lxml')

    print("Title", soup.title)
    links = soup.find_all('a')
    num_links = len(links)
    cx = 0


    for link in links:
        print("Link=", link)
        if '/albums/' in link.get('href', ''):
            print("href=", link.get('href'))
            album = link.get_text().strip()
            url = link.get('href')
            print("Will download the album {} at url {}".format(album,
                                                                url))
            print("Download {} of {}".format(cx, num_links))
            download_album(album, url)
        else:
            print("No albums found for link", link)
        cx += 1



def download_album(album, url):
    print("Downloading album", album, "from", url)
    content = requests.get(url).text
    soup = bs.BeautifulSoup(content, 'lxml')

    links = soup.find_all('a')
    for link in links:
        song = link.get_text().strip()
        url = link.get('href', '')
        if song !=''  and 'download' in url and '320' in song:
            print("Downloading", song, "from", url)
            album = album.replace(' ','-')
            album = album.replace('(', '')
            album = album.replace(')', '')
            album = album.replace('[', '')
            album = album.replace(']', '')
            album = album.replace('/', '')

            filename = album+'.zip'
            path='../songs/'+filename
            command = 'wget '+url+' -O ' + path
            if not os.path.exists(path):
                print("Running command", command)
                os.system(command)
                sleep_duration = 30+randint(30, 60)
                print("Sleeping for {} seconds to be nice to the server".format(sleep_duration))
                time.sleep(sleep_duration)

            else:
                print(path, "Exists. Hence skipping")



if __name__ == "__main__":
    #download_album('Ala', 'https://maamusiq.com/albums/ala-vaikunthapurramuloo-2019/')
    main()
