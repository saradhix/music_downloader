import requests
import bs4 as bs
import sys
import os
import time
import urllib.parse as parse

homepage='https://hindimp3.a2z3gp.com/indexw.php?dir=Hindi+MP3%2FHindi+Old+MP3%2FBest+Of+90s+VHQ%2FOld+Mp3+Collection+A+-+B&page='
baseurl=""
name='A-B'
max_pages=51
homepage='https://hindimp3.a2z3gp.com/indexw.php?dir=Hindi+MP3%2FHindi+Old+MP3%2FBest+Of+90s+VHQ%2FOld+Mp3+Collection+C+-+D&page='
name='C-D'
max_pages=51
homepage='https://hindimp3.a2z3gp.com/indexw.php?dir=Hindi+MP3%2FHindi+Old+MP3%2FBest+Of+90s+VHQ%2FOld+Mp3+Collection+E+-+I&page='
name='E-I'
max_pages=46
homepage='https://hindimp3.a2z3gp.com/indexw.php?dir=Hindi+MP3%2FHindi+Old+MP3%2FBest+Of+90s+VHQ%2FOld+Mp3+Collection+J+-+L+&page='
name='J-L'
max_pages=56
homepage='https://hindimp3.a2z3gp.com/indexw.php?dir=Hindi+MP3%2FHindi+Old+MP3%2FBest+Of+90s+VHQ%2FOld+Mp3+Collection+M+-+N&page='
name='M-N'
max_pages=53
homepage='https://hindimp3.a2z3gp.com/indexw.php?dir=Hindi+MP3%2FHindi+Old+MP3%2FBest+Of+90s+VHQ%2FOld+Mp3+Collection+O+-+P&page='
name='O-P'
max_pages=32
homepage='https://hindimp3.a2z3gp.com/indexw.php?dir=Hindi+MP3%2FHindi+Old+MP3%2FBest+Of+90s+VHQ%2FOld+Mp3+Collection+Q+-+R&page='
name='Q-R'
max_pages=36
homepage='https://hindimp3.a2z3gp.com/indexw.php?dir=Hindi+MP3%2FHindi+Old+MP3%2FBest+Of+90s+VHQ%2FOld+Mp3+Collection+S&page='
name='S-Z'
max_pages=55
base_dir = '../a2zsongshindi'
def main():
    for page in range(1,max_pages+1): 
        dynamic_page = homepage + str(page)
        print("Dynamic page=", dynamic_page)
        download_album(dynamic_page, name)


def download_page(url):
    url = url.replace("c=8","c=1000")  
    #Above hack is ito ensure all albums belonging to this 
    #category page come in 1 shot instead of pagination by 8 
    print("Downloading albums from  the page url", url)
    r = requests.get(url)
    print(r.status_code)
    soup = bs.BeautifulSoup(r.text,'lxml')
    links = soup.find_all('a')
    for link in links:
        if 'mp3' in link.get('href'):
            album = link.get_text().strip()
            url = link.get('href')
            print("URL=|", url, "|page=", album)
            download_album(url, album)

def download_album(url, album):
    album = album.replace('(','').replace(')','').replace('[','').replace(']','').replace(' ','_')
    print("Downloading album", album, "from", url)
    content = requests.get(url).text
    soup = bs.BeautifulSoup(content, 'lxml')

    links = soup.find_all('a', {"class":"fileName"})
    for link in links:
        print("Downloading link", link)
        song = link.get_text().strip()
        url = link.get('href')
        if url.endswith('.mp3') or True:
            print("Song=", song, "url=", url)
            try:
                os.mkdir(base_dir+'/'+album)
            except:
                pass
            download_song(baseurl+url, album, song)


def download_song(url, album, song):
    prefix='https://hindimp3.a2z3gp.com/down.php?file='
    print("Download ", song, "from album", album,"from url", url)
    #Modify the url to replace these things
    #download.php?d= should be replaced with to load/
    #&filename= should be replaced with /

    #url = url.replace("down.php?file=","")
    url = url.replace("&filename=", "")
    url = url.replace("/wfile/", "")
    url = url.replace(".html", "")
    #url = parse.quote(url)
    #print(url)
    song = song.replace(' ', '_')
    song = song.replace('(','').replace(')','').replace('[','').replace(']','').replace(' ','_')
    #print("Replaced url=", url, "song=",song)
    path = base_dir+'/'+album+'/'+song
    print("Downloading to path", path)
    command = "wget '"+prefix+url+"' -O " + path
    if not os.path.exists(path):
        print("Running command", command)
        os.system(command)
        time.sleep(5)
    else:
        print(path, "Exists. Hence skipping")


if __name__ == "__main__":
    #download_album('Ala', 'https://maamusiq.com/albums/ala-vaikunthapurramuloo-2019/')
    main()
